# Dispositivos móveis

## Pós-graduação em Desenvolvimento de Sistemas - CEFET-MG Unidade Varginha - 2019

Este é o repositório oficial da disciplina de Dispositivos Móveis da Pós-graduação em Desenvolvimento de Sistemas do CEFET-MG Unidade Varginha, turma 2019-2020.

Aqui, serão postados materiais, exercícios e documentos que serão usados tanto em aula quanto como referência para os alunos.